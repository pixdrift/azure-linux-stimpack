#!/bin/bash

# ---
# Create and enable an 8G swap file on boot using Azure ephermeral disk
# For cloud-init, place in /var/lib/cloud/scripts/per-boot/ and chmod +x
# ---

# Determine if the Azure ephemeral disk is mounted
if findmnt -bfn /dev/disk/cloud/azure_resource-part1 &>/dev/null; then

  # Determine mount point and define swap file location in root of ephemeral drive
  AZMOUNT=$(findmnt -bfn /dev/disk/cloud/azure_resource-part1 -o TARGET)
  SWAPFILE="${AZMOUNT}/swapfile"

  # If the swap file doesn't exist, create an empty file with correct permission and initialise as swap
  if [ ! -f "${SWAPFILE}" ]; then
    fallocate --length 8G ${SWAPFILE}
    chmod 600 ${SWAPFILE}
    mkswap ${SWAPFILE}
  fi

  # Mount the swap file if it isn't already mounted
  swapon | grep -q "^${SWAPFILE}" || swapon ${SWAPFILE}
fi
